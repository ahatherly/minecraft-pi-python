class MazeNode:
    has_wall = False
    wall_right = False
    wall_down = False
    def __init__(self, wall_right, wall_down):
        self.wall_right=wall_right
        self.wall_down=wall_down
        self.has_wall=(wall_right or wall_down)
    def set_wall(self, val):
        self.has_wall=val
