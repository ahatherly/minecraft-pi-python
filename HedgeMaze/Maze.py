import sys
import random
from MazeNode import MazeNode
from mcpi import minecraft

mc = minecraft.Minecraft.create()

def make_wall(direction, node_pos, free_nodes, blocks, test):
    x = node_pos % size_x
    y = int(node_pos) / int(size_x)
    wall_length = 0;
    if direction == 0:
        # Right
        while blocks[x][y].has_wall == False:
            n = blocks[x][y]
            wall_length = wall_length + 1
            if not test:
                n.wall_right = True
                n.has_wall = True
                free_nodes.remove(x+y*size_y)
            x = x + 1
    elif direction == 1:
        # Down
        while blocks[x][y].has_wall == False:
            n = blocks[x][y]
            wall_length = wall_length + 1
            if not test:
                n.wall_down = True
                n.has_wall = True
                free_nodes.remove(x+y*size_y)
            y = y + 1
    elif direction == 2:
        # Left
        blocks[x][y].has_wall = True
        wall_length = wall_length + 1
        if not test:
            free_nodes.remove(x+y*size_y)
        had_wall = False
        while had_wall == False:
            x = x - 1
            n = blocks[x][y]
            had_wall = n.has_wall
            wall_length = wall_length + 1
            if not test:
                n.wall_right = True
                n.has_wall = True
                if not had_wall:
                    free_nodes.remove(x+y*size_y)
    elif direction == 3:
        # Up
        blocks[x][y].has_wall = True
        wall_length = wall_length + 1
        if not test:
            free_nodes.remove(x+y*size_y)
        had_wall = False
        while had_wall == False:
            y = y - 1
            n = blocks[x][y]
            had_wall = n.has_wall
            wall_length = wall_length + 1
            if not test:
                n.wall_down = True
                n.has_wall = True
                if not had_wall:
                    free_nodes.remove(x+y*size_y)
    return wall_length

size_x = 15
size_y = size_x
max_wall = 3

blocks = [[0 for x in range(size_x)] for y in range(size_y)]
free_nodes = []

# Start with an empty maze
for y in range(0,size_y):
    for x in range (0,size_x):
        if x==0 and y==0:
            # Top left
            blocks[x][y] = MazeNode(True, True)
        elif x==0 and y==size_y-1:
            # Bottom left
            blocks[x][y] = MazeNode(True, False)
        elif x==size_x-1 and y==0:
            # Top right
            blocks[x][y] = MazeNode(False, True)
        elif x==size_x-1 and y==size_y-1:
            # Bottom right
            n=MazeNode(False, False)
            n.set_wall(True)
            blocks[x][y] = n
        elif x==0 or x==size_x-1:
            # Left and Right Edge
            blocks[x][y] = MazeNode(False, True)
        elif y==0 or y==size_y-1:
            # Top and Bottom Edge
            blocks[x][y] = MazeNode(True, False)
        else:
            blocks[x][y] = MazeNode(False, False)
            free_nodes.append(x+y*size_y)

# Make and entrance and an exit
entrance = blocks[0][0]
entrance.wall_down = False
exit = blocks[size_x-1][size_y-2]
exit.wall_down = False

# Now, generate the maze
while len(free_nodes)>0:
    random.shuffle(free_nodes)
    direction = random.randint(0,4)
    node_pos = free_nodes[0]
    wall_length = make_wall(direction, node_pos, free_nodes, blocks, True)
    if wall_length < max_wall:
        make_wall(direction, node_pos, free_nodes, blocks, False)
"""
# Draw the maze
for y in range(0,size_y):
    for print_row in range(0,3):
        for x in range (0,size_x):
            node = blocks[x][y]
            if print_row==0:
                if node.wall_right:
                    sys.stdout.write('#--')
                elif node.has_wall:
                    sys.stdout.write('#  ')
                else:
                    sys.stdout.write('   ')
            if print_row>0:
                if node.wall_down:
                    sys.stdout.write('|  ')
                else:
                    sys.stdout.write('   ')
        print ''

print 'Free nodes {0}'.format(len(free_nodes))
"""

# Draw a base for the maze
player_x, player_y, player_z = mc.player.getPos()
air = 0
floor = 2
walls = 18

mc.setBlocks(player_x+1, player_y-1, player_z+1, \
             player_x+(size_x*3)+1, player_y-1, player_z+(size_x*3)+1, floor)

# Draw the maze
for z in range(0,size_y):
    for print_row in range(0,3):
        for x in range (0,size_x):
            mine_x = player_x+1+(x*3)
            mine_z = player_z+1+(z*3)
            node = blocks[x][z]
            if print_row==0:
                if node.wall_right:
                    mc.setBlock(mine_x, player_y, mine_z, walls)
                    mc.setBlock(mine_x+1, player_y, mine_z, walls)
                    mc.setBlock(mine_x+2, player_y, mine_z, walls)
                    mc.setBlock(mine_x, player_y+1, mine_z, walls)
                    mc.setBlock(mine_x+1, player_y+1, mine_z, walls)
                    mc.setBlock(mine_x+2, player_y+1, mine_z, walls)
                    #sys.stdout.write('#--')
                elif node.has_wall:
                    mc.setBlock(mine_x, player_y, mine_z, walls)
                    mc.setBlock(mine_x+1, player_y, mine_z, air)
                    mc.setBlock(mine_x+2, player_y, mine_z, air)
                    mc.setBlock(mine_x, player_y+1, mine_z, walls)
                    mc.setBlock(mine_x+1, player_y+1, mine_z, air)
                    mc.setBlock(mine_x+2, player_y+1, mine_z, air)
                    #sys.stdout.write('#  ')
                else:
                    mc.setBlock(mine_x, player_y, mine_z, air)
                    mc.setBlock(mine_x+1, player_y, mine_z, air)
                    mc.setBlock(mine_x+2, player_y, mine_z, air)
                    mc.setBlock(mine_x, player_y+1, mine_z, air)
                    mc.setBlock(mine_x+1, player_y+1, mine_z, air)
                    mc.setBlock(mine_x+2, player_y+1, mine_z, air)
                    #sys.stdout.write('   ')
            if print_row>0:
                if node.wall_down:
                    mc.setBlock(mine_x, player_y, mine_z+print_row, walls)
                    mc.setBlock(mine_x+1, player_y, mine_z+print_row, air)
                    mc.setBlock(mine_x+2, player_y, mine_z+print_row, air)
                    mc.setBlock(mine_x, player_y+1, mine_z+print_row, walls)
                    mc.setBlock(mine_x+1, player_y+1, mine_z+print_row, air)
                    mc.setBlock(mine_x+2, player_y+1, mine_z+print_row, air)
                    #sys.stdout.write('|  ')
                else:
                    mc.setBlock(mine_x, player_y, mine_z+print_row, air)
                    mc.setBlock(mine_x+1, player_y, mine_z+print_row, air)
                    mc.setBlock(mine_x+2, player_y, mine_z+print_row, air)
                    mc.setBlock(mine_x, player_y+1, mine_z+print_row, air)
                    mc.setBlock(mine_x+1, player_y+1, mine_z+print_row, air)
                    mc.setBlock(mine_x+2, player_y+1, mine_z+print_row, air)
                    #sys.stdout.write('   ')
print "Done"
